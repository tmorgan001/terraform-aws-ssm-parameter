
output "json-parameter-mappings" {
  value = [
      for parameter, arn in zipmap(
      sort(keys(var.application_parameters_map)),
      sort(values(aws_ssm_parameter.application-parameter)[*]["arn"])) :
      tomap({"name"= parameter, "valueFrom"= arn})
  ]
}


