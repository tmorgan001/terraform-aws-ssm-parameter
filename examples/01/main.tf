/*
A map is created with the parameters to be created. 
This example shows three parameters:
    APP_VERSION: "9.5.5"
    DB_USER: "db_usr"
    DB_PASSWORD: "p@ssword1"    (as a SecureString)

*/
variable "my-app-param-map" {
  type = map
  default = {
    "APP_VERSION" = {
      "description" = "The application version"
      "value" = "9.5.5"
      "secret" = false
    }
    "DB_USER" = {
      "description" = "The username for the database"
      "value" = "db_user"
      "secret" = false
    }
    "DB_PASSWORD" = {
      "description" = "The database password"
      "secret" = true
      "value" = "p@ssword1"
    }
  }
}


/*
The parameters are passed to the module into the 
'application_parameters_map' variable. 
The three parameters are then created in SSM parameters.
*/
module "my-app-application-parameters" {
  source              = "../../"
  name                = "my-app"
  application_parameters_map = var.my-app-param-map
}

/*
The output is in JSON format that is required in a task
definition file. The output contains the name of the parameter 
the ARN.
*/
output "json-parameter-mappings" {
  value = module.my-app-application-parameters.json-parameter-mappings
}
