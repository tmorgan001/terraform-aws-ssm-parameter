variable "application_parameters" {
    description = "list of application parameters"
    type        = list
    default     = []
}

variable "name" {
    description = "the name of your application to prepend to secret names in AWS Secrets Manager"
    type        = string
    default     = "application name"
}

variable "application_parameters_map" {
    type = map
}

variable "key" {
    description = "KMS key to encrypt secrets. May also be specific per parameter."
    type = string
    default = "alias/aws/ssm"
}