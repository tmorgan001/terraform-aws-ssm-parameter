data "aws_kms_key" "ssm_key" {
  key_id = var.key
}


resource "aws_ssm_parameter" "application-parameter" {
    for_each = var.application_parameters_map
    type = each.value["secret"] ? "SecureString" : "String"
    name = "${var.name}/${each.key}"
    value = each.value["value"]
    description = each.value["description"]
    key_id = each.value["secret"] == true ? lookup(each.value, "key", data.aws_kms_key.ssm_key.key_id) : null
    tags = {
        Terraform = "true"
        Secret = each.value["secret"]
    }
}


