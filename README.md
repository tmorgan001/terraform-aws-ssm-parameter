# terraform-aws-ssm-parameters

The **terraform-aws-ssm-parameters** module creates AWS SSM Parameters. It takes in a map of parameter names and values to create the parameters. It can also output JSON format intended for a container definition file.

## Example Usage



```
variable "my-app-param-map" {
  type = map
  default = {
    "APP_VERSION" = {
      "description" = "The application version"
      "value" = "9.5.5"
      "secret" = false
    }
    "DB_USER" = {
      "description" = "The username for the database"
      "value" = "db_user"
      "secret" = false
    }
    "DB_PASSWORD" = {
      "description" = "The database password"
      "secret" = true
      "value" = "p@ssword1"
    }
  }
}

module "my-app-application-parameters" {
  source              = "bitbucket.org/tmorgan001/terraform-aws-ssm-parameter"
  name                = "my-app"
  application_parameters_map = var.my-app-param-map
}

output "json-parameter-mappings" {
  value = module.my-app-application-parameters.json-parameter-mappings
}
```

## JSON Output
```
Outputs:

json-parameter-mappings = [
  {
    "name" = "APP_VERSION"
    "valueFrom" = "arn:aws:ssm:us-east-1:123465176223:parameter/my-app-APP_VERSION"
  },
  {
    "name" = "DB_USER"
    "valueFrom" = "arn:aws:ssm:us-east-1:123465176223:parameter/my-app-DB_NAME"
  },
  {
    "name" = "DB_PASSWORD"
    "valueFrom" = "arn:aws:ssm:us-east-1:123465176223:parameter/my-app-DB_VERSION"
  },
]
```